package com.aram.authservice.dto;

import com.aram.authservice.model.UserRole;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author aram
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    // the user id
    private Long id;

    // the user name
    private String name;

    // the user surname
    private String surname;

    // the user email
    private String email;

    // the user password
    private String password;

    @Column(name = "user_role")
    @Enumerated(value = EnumType.STRING)
    // the user role
    private UserRole userRole;

    // is user locked
    private Boolean locked;

    // is user enabled
    private Boolean enabled;
    
}
