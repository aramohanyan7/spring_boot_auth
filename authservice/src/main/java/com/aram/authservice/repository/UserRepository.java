package com.aram.authservice.repository;

import com.aram.authservice.model.User;
import java.util.Optional;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author aram
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    
    // find user by given email
    Optional<User> findByEmail(String email);    
}
