package com.aram.authservice.service;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.aram.authservice.model.User;
import com.aram.authservice.repository.UserRepository;
import java.text.MessageFormat;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.aram.authservice.dto.UserDTO;
import com.aram.authservice.model.UserRole;

/**
 *
 * @author aram
 */
@Service
public class UserService implements UserDetailsService {

    @Autowired
    // the user repository
    private UserRepository userRepository;
    
    @Autowired
    // the password encoder
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * Find user by given email or throw exception.
     * @param email
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        final Optional<User> user = this.userRepository.findByEmail(email);

        return user.orElseThrow(() -> new UsernameNotFoundException(MessageFormat.format("User with email {0} cannot be found.", email)));
    }

    /**
     * Create new user from given userDTO object.
     * @param userDTO
     * @return
     */
    public Optional<UserDTO> create(UserDTO userDTO) {

        // convert userDTO to User object
        User user = this.userDTOToUser(userDTO);

        // save the user
        final User createdUser = userRepository.save(user);

        // convert saved user object to userDTO
        UserDTO createdUserDTO = this.userToUserDTO(createdUser);
        
        // return optional of userDTO object
        return Optional.ofNullable(createdUserDTO);

    }
    
    /**
     * Convert userDTO to User object.
     * TODO move to mapper component.
     * @param userDTO
     * @return
     */
    private User userDTOToUser(UserDTO userDTO) {
        User user = new User();
        
        user.setEmail(userDTO.getEmail());
        user.setEnabled(Boolean.TRUE);
        user.setLocked(Boolean.FALSE);
        user.setName(userDTO.getName());
        user.setPassword(this.bCryptPasswordEncoder.encode(userDTO.getPassword()));
        user.setSurname(userDTO.getSurname());
        user.setUserRole(UserRole.USER);
        
        return user;
    }
    
    /**
     * Convert user to UserDTO object.
     * TODO move to mapper component.
     * @param userDTO
     * @return
     */
    private UserDTO userToUserDTO(User user) {
        UserDTO userDTO = new UserDTO();
        
        userDTO.setId(user.getId());
        userDTO.setEmail(user.getEmail());
        userDTO.setName(user.getName());
        userDTO.setSurname(user.getSurname());
        userDTO.setUserRole(user.getUserRole());
        
        return userDTO;
    }

}
