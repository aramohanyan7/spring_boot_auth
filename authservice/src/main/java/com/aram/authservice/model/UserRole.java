package com.aram.authservice.model;

/**
 * The enum to list user's roles.
 * @author aram
 */
public enum UserRole {
    ADMIN, USER
}
