package com.aram.authservice.model;

import java.util.Collection;
import java.util.Collections;
import javax.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 *
 * @author aram
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "auth_user")
public class User implements UserDetails {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // the user id
    private Long id;

    // the user name
    private String name;

    // the user surname
    private String surname;

    // the user email
    private String email;

    // the user password
    private String password;

    @Column(name = "user_role")
    @Enumerated(value = EnumType.STRING)
    // the user role
    private UserRole userRole;

    // is user locked
    private Boolean locked;

    // is user enabled
    private Boolean enabled = true;

    /**
     * Get user authorities
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        final SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(this.userRole.name());
        return Collections.singletonList(simpleGrantedAuthority);
    }

    /**
     * Get user password.
     * @return
     */
    @Override
    public String getPassword() {
        return this.password;
    }

    /**
     * Get the username.
     * @return
     */
    @Override
    public String getUsername() {
        return this.email;
    }

    /**
     * Check if account is expired.
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Check if account non locked.
     * @return
     */
    @Override
    public boolean isAccountNonLocked() {
        return !this.locked;
    }

    /**
     * Check if credentials non expired.
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Check if account is enabled.
     * @return
     */
    @Override
    public boolean isEnabled() {
        return this.enabled;
    }
    
}
