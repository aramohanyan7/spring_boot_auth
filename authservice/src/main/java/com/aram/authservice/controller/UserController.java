package com.aram.authservice.controller;

import com.aram.authservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.aram.authservice.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;

/**
 *
 * @author aram
 */
@RestController
@RequestMapping("/api/v1/user")
@Slf4j
public class UserController {
    
    @Autowired
    // the user service
    private UserService userService;
    
    /**
     * The endpoint to create new user. 
     * @param user
     * @return
     */
    @PostMapping("/sign-up")
    public ResponseEntity<?> signUp(
        @RequestBody UserDTO user
    ) {
        // create new user
        Optional<UserDTO> userDTO = this.userService.create(user);
        
        // check if the object is not present throw exception
        // TODO find more properly way
        if(!userDTO.isPresent()) {
            throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok().body(userDTO.get());
    }
        
    /**
     * The method to use for resource servers.
     * @param user
     * @return
     */
    @PostMapping("/details")
    public Map<String, Object> user(OAuth2Authentication user) {
        log.info("user {}", user);
        Map<String, Object> userInfo = new HashMap<>();
        // put the user principal
        userInfo.put("user", user.getUserAuthentication().getPrincipal());
        // put the user authorities
        userInfo.put("authorities", AuthorityUtils.authorityListToSet(
                user.getUserAuthentication().getAuthorities()
            )
        );
        return userInfo;
    }
}
