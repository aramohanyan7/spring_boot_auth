# spring boot auth #

The simple project to demonstrate spring boot auth

### How to run ###

* mvn clean install -DskipTests=True
* docker-compose  up --build

### How to use ###

* To get the JWT token for the ADMIN user or change the email and password for another user:
curl -X POST \
  'http://localhost:8080/oauth/token?grant_type=password&scope=webclient&username=admin@admin.com&password=password' \
  -H 'Authorization: Basic Y2xpZW50OnNlY3JldA==' \
  -H 'Content-Type: application/json' 

* To creaet new user:
curl -X POST \
  http://localhost:8080/api/v1/user/sign-up \
  -H 'Content-Type: application/json' \
  -d '{
	"name": "name2",
	"surname": "surname2",
	"email": "email@email.email",
	"password": "password",
	"userRole": "USER"
	}'

* To list all products:
curl -X GET \
  'http://localhost:8081/api/v1/product?name=name2' \
  -H 'Authorization: Bearer the token' \
  -H 'Content-Type: application/json'

* To create new product:
curl -X POST \
  http://localhost:8081/api/v1/product \
  -H 'Authorization: Bearer the token' \
  -H 'Content-Type: application/json' \
  -d '{
    "name": "name2",
    "price": 153.54,
    "range": "range",
    "rate": null,
    "description": null
    }'

* To update existing product:
curl -X PUT \
  http://localhost:8081/api/v1/product \
  -H 'Authorization: Bearer the token' \
  -H 'Content-Type: application/json' 
  -d '{
    "id": 1,
    "name": "name",
    "price": 11.54,
    "range": "range"
    }'

* To delete the product:
curl -X DELETE \
  'http://localhost:8081/api/v1/product?id=2' \
  -H 'Authorization: Bearer the token' \
  -H 'Content-Type: application/json' 

* To rate the product:
curl -X PUT \
  http://localhost:8081/api/v1/product/rate \
  -H 'Authorization: Bearer the token' \
  -H 'Content-Type: application/json' 
  -d '{
    "id": 1,
    "description": "description",
    "rate": 11
    }'
