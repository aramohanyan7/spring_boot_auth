DROP TABLE IF EXISTS shop_product;

CREATE TABLE shop_product (
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(50) NULL,
    price NUMERIC(5,2) NULL,
    range VARCHAR(50) NULL,
    rate INT NULL,
    description TEXT NULL,
    is_deleted BOOLEAN NOT NULL DEFAULT FALSE
);
