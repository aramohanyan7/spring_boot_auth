package com.aram.shopservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 *
 * @author aram
 */
@Component
@Configuration
public class ServiceConfig {
    
    @Value("${signing.key}")
    // the JWT signing key
    private String jwtSigningKey;

    /**
     * Return the JWT signing key from config.
     * @return
     */
    public String getJwtSigningKey() {
        return jwtSigningKey;
    }
    
}
