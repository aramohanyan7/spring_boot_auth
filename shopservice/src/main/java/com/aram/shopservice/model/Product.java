package com.aram.shopservice.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author aram
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "shop_product")
public class Product implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // the product id
    private Long id;

    // the product name
    private String name;
    
    // the product price
    private BigDecimal price;
    
    // the product range
    private String range;
    
    // the product rate
    private Integer rate;
    
    // the product description
    private String description;
    
    @Column(name = "is_deleted")
    // is deleted
    private Boolean isDeleted;
    
}
