package com.aram.shopservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.aram.shopservice.model.Product;
import java.util.List;

/**
 *
 * @author aram
 */
@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
    
    // find all products by name and is deleted flag
    List<Product> findByNameAndIsDeleted(String name, Boolean isDeleted);
    
}
