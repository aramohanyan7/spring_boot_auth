package com.aram.shopservice.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author aram
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RateProductDTO implements Serializable {
    
    // the product id
    private Long id;
    
    // the product rate
    private Integer rate;
    
    // the product description
    private String description;
    
}
