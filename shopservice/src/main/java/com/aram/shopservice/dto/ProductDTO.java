package com.aram.shopservice.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author aram
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO implements Serializable {
    
    // the product id
    private Long id;

    // the product name
    private String name;
    
    // the product price
    private BigDecimal price;
    
    // the product range
    private String range;
    
    // the product rate
    private Integer rate;
    
    // the product description
    private String description;
    
    // is deleted
    private Boolean isDeleted = Boolean.FALSE;
    
}