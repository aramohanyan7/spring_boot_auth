package com.aram.shopservice.service;

import com.aram.shopservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aram.shopservice.model.Product;
import com.aram.shopservice.dto.ProductDTO;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javassist.NotFoundException;
import com.aram.shopservice.dto.RateProductDTO;

/**
 *
 * @author aram
 */
@Service
public class ProductService {
    
    @Autowired
    // the product repository
    private ProductRepository productRepository;
    
    /**
     * Get all products by given name.
     * @param name
     * @return
     */
    public List<ProductDTO> getList(
        String name
    ) {
        List<Product> products = this.productRepository.findByNameAndIsDeleted(name, Boolean.FALSE);
        
        return products
                .stream()
                .map(product -> {return this.productToProductDTO(product);})
                .collect(Collectors.toList());
    }
    
    /**
     * Save the Product object.
     * @param productDTO
     * @return
     */
    public ProductDTO save(ProductDTO productDTO) {
        Product product = this.productDTOToProduct(productDTO);
        
        Product createdProduct = this.productRepository.save(product);
        
        return this.productToProductDTO(createdProduct);
    }
    
    /**
     * Delete the Product object.
     * @param id
     * @return
     * @throws javassist.NotFoundException
     */
    public ProductDTO delete(Long id) throws NotFoundException {
        Optional<Product> product = this.productRepository.findById(id);
        
        // if product does not exist throw exception
        // TODO find more proper way to handle this case
        if(!product.isPresent()) {
            throw new NotFoundException("The product not found by given id " + id);
        }
        
        // mark the product as deleted
        product.get().setIsDeleted(Boolean.TRUE);
        Product savedProduct = this.productRepository.save(product.get());
        return this.productToProductDTO(savedProduct);
    }
    
    /**
     * Rate the product.
     * @param rateProductDTO
     * @return
     * @throws NotFoundException
     */
    public ProductDTO rateProduct(RateProductDTO rateProductDTO) throws NotFoundException {
        Optional<Product> product = this.productRepository.findById(rateProductDTO.getId());
        
        // if product does not exist throw exception
        // TODO find more proper way to handle this case
        if(!product.isPresent()) {
            throw new NotFoundException("The product not found by given id " + rateProductDTO.getId());
        }
        
        product.get().setRate(rateProductDTO.getRate());
        product.get().setDescription(rateProductDTO.getDescription());
        
        Product savedProduct = this.productRepository.save(product.get());
        return this.productToProductDTO(savedProduct);
    }
    
    /**
     * Map product to productDTO.
     */
    private ProductDTO productToProductDTO(Product product) {
        ProductDTO productDTO = new ProductDTO();
        
        productDTO.setId(product.getId());
        productDTO.setName(product.getName());
        productDTO.setDescription(product.getDescription());
        productDTO.setPrice(product.getPrice());
        productDTO.setRate(product.getRate());
        productDTO.setRange(product.getRange());
        
        return productDTO;
    }
    
    /**
     * Map productDTO to product.
     */
    private Product productDTOToProduct(ProductDTO productDTO) {
        Product product = new Product();
        
        product.setName(productDTO.getName());
        product.setDescription(productDTO.getDescription());
        product.setPrice(productDTO.getPrice());
        product.setRate(productDTO.getRate());
        product.setRange(productDTO.getRange());
        product.setIsDeleted(productDTO.getIsDeleted());
        
        return product;
    }
    
}
