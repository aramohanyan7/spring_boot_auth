package com.aram.shopservice.controller;

import com.aram.shopservice.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.aram.shopservice.dto.ProductDTO;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import com.aram.shopservice.dto.RateProductDTO;

/**
 *
 * @author aram
 */
@RestController
@RequestMapping("/api/v1/product")
@Slf4j
public class ProductController {
    
    @Autowired
    // the product service
    private ProductService productService;
    
    /**
     * The endpoint to list all products.
     * @param name
     * @return
     */
    @GetMapping("")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<?> get(
        @RequestParam String name
    ) {
        return ResponseEntity.ok(
            this.productService.getList(name)
        );
    }
    
    /**
     * The endpoint to create new product by admin.
     * @param product
     * @return
     */
    @PostMapping("")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> create(
        @RequestBody ProductDTO product
    ) {
        return ResponseEntity.status(HttpStatus.CREATED).body(
            this.productService.save(product)
        );
    }
    
    /**
     * The endpoint to update the product by admin.
     * @param product
     * @return
     */
    @PutMapping("")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> update(
        @RequestBody ProductDTO product
    ) {
        return ResponseEntity.ok(
            this.productService.save(product)
        );
    }
    
    /**
     * The endpoint to delete the product by admin.
     * @param id
     * @return
     * @throws javassist.NotFoundException
     */
    @DeleteMapping("")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> delete(
        @RequestParam Long id
    ) throws NotFoundException {
        this.productService.delete(id);
        
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }
    
    /**
     * The endpoint to rate the product by admin or user.
     * @param rateProductDTO
     * @return
     * @throws javassist.NotFoundException
     */
    @PutMapping("/rate")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('ADMIN')")
    public ResponseEntity<?> rate(
        @RequestBody RateProductDTO rateProductDTO
    ) throws NotFoundException {
        return ResponseEntity.ok(
            this.productService.rateProduct(rateProductDTO)
        );
    }
    
}
